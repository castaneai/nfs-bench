# nfs-bench

NFS Server Read-only Benchmark for k8s volume mount

```sh
# In the NFS Server
# mount disk to /mnt/filestorage
# create dummy file
dd if=/dev/zero of=/mnt/filestorage/tempfile bs=1M count=1024 conv=fdatasync,notrunc status=progress
```

- Setup Kubernetes cluster with the same VPC as the NFS server.
- Edit `job.yaml` and put the private IP of the NFS server to `volumes:`.
- Deploy `job.yaml` and start benchmarking.

```sh
kubectl apply -f job.yaml
```
